# Code of Conduct

[[_TOC_]]

## Introduction
As contributors and maintainers of the Picnic project, we are committed to creating a friendly and inclusive community for everyone. We value the participation of individuals from different backgrounds and perspectives and want to ensure a positive experience for all community members.

This Code of Conduct outlines our expectations for those who participate in our project and provides guidelines for respectful and inclusive behavior. It applies to all project-related spaces, including but not limited to GitHub repositories, issue trackers, social media channels, and community events.

## Our Pledge
In the interest of fostering an open and welcoming environment, we pledge to:

- Be respectful and considerate towards others.
- Listen to and value diverse opinions and experiences.
- Be open to constructive feedback and ideas.
- Be inclusive and strive to provide equal opportunities for participation.
- Avoid any form of harassment, discrimination, or offensive behavior.

## Expected Behavior
All community members, including contributors, maintainers, and users, are expected to:

- Be courteous and respectful in interactions.
- Use inclusive language and avoid derogatory or offensive remarks.
- Accept constructive criticism gracefully and provide constructive feedback.
- Be mindful of the diversity of the community and refrain from any form of harassment or discrimination.
- Respect the privacy and personal boundaries of others.
- Be patient and understanding towards others' viewpoints and experiences.
- Foster a collaborative and supportive community.

## Unacceptable Behavior
The following behaviors are considered unacceptable and will not be tolerated:

- Harassment, discrimination, or any form of offensive behavior based on race, gender, age, sexual orientation, disability, nationality, or other protected characteristics.
- Personal attacks or insults, either in public or private communications.
- Trolling, derogatory comments, or any disruptive behavior that hinders a positive community environment.
- Any form of unwelcome advances, inappropriate or sexualized remarks, or unwarranted personal attention.
- Any action that violates applicable laws or regulations.

## Consequences of Unacceptable Behavior
If a community member engages in behavior that violates this Code of Conduct, the project maintainers may take appropriate action, including but not limited to issuing warnings, temporary or permanent bans from the project's platforms, or other actions deemed necessary to maintain a positive and inclusive community.

## Reporting Guidelines
If you witness or experience any behavior that violates this Code of Conduct, please report it to the project maintainers by contacting (TBD) (or other appropriate contact method). All reports will be handled confidentially and with the utmost respect for the privacy of the individuals involved. We will investigate the issue promptly and take appropriate action.

## Attribution
This Code of Conduct is adapted from the Contributor Covenant, version 2.0, available [here](https://www.contributor-covenant.org/version/2/0/code_of_conduct.html).

## Conclusion
By participating in the Picnic project, you are expected to uphold this Code of Conduct. We appreciate your cooperation in making our community inclusive and welcoming to all.
