# Contributing to Picnic

Thank you for your interest in contributing to Picnic! We appreciate any contributions that help improve our project. Please take a moment to review the following guidelines before getting started.

[[_TOC_]]


## Code of Conduct
Please note that we have a [Code of Conduct](CODE_OF_CONDUCT.md) in place to ensure a positive and inclusive environment for everyone. By participating in this project, you agree to abide by its terms.

## How to Contribute
1. Fork the repository and create your branch from `main`.
1. Make your changes and ensure that the code is properly tested.
1. Include clear and descriptive commit messages.
1. Submit a pull request to the `main` branch, explaining the changes you have made.

We don't have a formal process for handling contributions at the moment, but we still welcome your contributions! We will do our best to review and merge them in a timely manner. We appreciate your patience and understanding.

## Development Setup
If you need help setting up the development environment or have any questions, please refer to the [README.md](README.md) file for general information about the project.

Thank you for your contributions!
