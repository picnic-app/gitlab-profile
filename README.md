# Picnic

<div align="center">
  ![Picnic Logo](assets/img/logo.png){width=30%}
</div>

[[_TOC_]]

## Description

We are at our best when we’re connected -- with our passions, our world, and with each other! Let Picnic be the social media destination for getting YOU out there to communities that share your interests and love them just as much as you. Picnic makes it easy to dive directly into feeds centered around your interests, toggle between those interest feeds, and find exactly what you’re in the mood for. Share fun short videos, pictures, and messages with others in your interest groups! No matter what you love (from writing to scuba diving to fashion), Picnic has a home for you.

We are proud to make our code open source and hope that the community's contributions will help make Picnic even better!

## Installation

There are no installation instructions available at the moment. We will provide them in the future.

## Usage

There are no specific usage instructions available at the moment. We will provide them in the future.

## License

All repositories under @picnic-app namespace are licensed under the [GNU Affero General Public License v3.0](https://opensource.org/licenses/AGPL-3.0). See the [LICENSE](LICENSE) file in each repository for more details.

## Contributing

We welcome contributions from the community to make Picnic even better! Please check out [our contributing guideluines](CONTRIBUTING.md).
Thank you for your interest in contributing to Picnic!

## Security

We take the security of Picnic seriously. If you discover any security-related issues or potential vulnerabilities while reviewing the source code, we appreciate your cooperation in responsibly disclosing it to us. To report a security issue:

- Create a confidential issue in this repository if applicable.
- OR, send us an email to [opensource-security@picnic.zone](mailto:opensource-security@picnic.zone).

We will investigate and address the issue promptly. We value your efforts in keeping Picnic secure.

## Namespace structure

- @picnic-app
  - @picnic-app/backend - directory for our backend services projects
    - @picnic-app/backend/libs - directory for our libraries projects for backend services
  - @picnic-app/devops - directory for our CI-related projects
    - picnic-app/devops/cicd-lib> - project for our CI templates
  - @picnic-app/frontend - directory for our frontend services projects
  - @picnic-app/pods - directory for our pods projects
  - @picnic-app/user-groups - directory for groups that are used to share members
  - picnic-app/gitlab-profile> - the readme project